// License: MIT
// Original Author: YUKI "Piro" Hiroshi <yuki@clear-code.com>

// edited by ytoune <tone@re-knock.io>

function uniqByIndexOf(array) {
  const uniquedArray = [];
  for (const elem of array) {
    if (uniquedArray.indexOf(elem) < 0)
      uniquedArray.push(elem);
  }
  return uniquedArray;
}

function uniqByIncludes(array) {
  const uniquedArray = [];
  for (const elem of array) {
    if (!uniquedArray.includes(elem))
      uniquedArray.push(elem);
  }
  return uniquedArray;
}

function uniqByFilter(array) {
  return array.filter(function(elem, index, self) {
    return self.indexOf(elem) === index;
  });
}

function uniqByFilterArrow(array) {
  return array.filter((elem, index, self) => self.indexOf(elem) === index);
}

function uniqByMap(array) {
  const knownElements = new Map();
  const uniquedArray = [];
  for (const elem of array) {
    if (knownElements.has(elem))
      continue;
    uniquedArray.push(elem);
    knownElements.set(elem, true);
  }
  return uniquedArray;
}

function uniqByMapKeys(array) {
  const knownElements = new Map();
  for (const elem of array) {
    knownElements.set(elem, true);
  }
  return Array.from(knownElements.keys());
}

function uniqBySet(array) {
  return Array.from(new Set(array));
}

function uniqBySetAndSpread(array) {
  return [...new Set(array)];
}

function uniqDedup(array) {
  const arr = array.slice()//.sort()
  let w = 0
  for (let i = 0, len = arr.length; i < len; ++i) {
    if (0 === w || arr[w - 1] !== arr[i]) {
      const tmp = arr[w]
      arr[w] = arr[i]
      arr[i] = tmp
      w++
    }
  }
  return arr.slice(0, w)
}

async function test(times, length, methods) {

  function prepareArray(length) {
    const array = [];
    for (let i = 0; i < length; i++) {
      array.push(parseInt(Math.random() * (length / 10)));
    }
    return array.sort();
  }

  const results = [];
  for (const uniq of methods) {
    const start = Date.now();
    const array = prepareArray(length);
    for (let i = 0; i < times; i++) {
      uniq(Array.from(array));
    }
    const delta = Date.now() - start;
    // console.log(`${times}times, length=${length}, ${uniq.name}`, delta);
    results.push(delta);
    await new Promise(resolve => setTimeout(resolve, 10));
  }
  return results;
}

!(async () => {
  const methods = [uniqByIndexOf, uniqByIncludes, uniqByFilter, uniqByFilterArrow, uniqByMap, uniqByMapKeys, uniqBySet, uniqBySetAndSpread, uniqDedup];
  const methodsnames = ',' + methods.map(f => f.name).join(',');

  async function runForTimes(length, start, step) {
    console.log(`length=${length}, start=${start}, step=${step}`)
    const results = [methodsnames];
    for (let i = 0, maxi = 5; i < maxi; i++) {
      const times = start + (step * i);
      results.push(times + ',' + (await test(times, length, methods)).join(','));
    }
    console.log(results.join('\n'));
  }

  console.log('tests with small array');
  await runForTimes(10, 100000, 100000);
  console.log('tests with middle array');
  await runForTimes(300, 2500, 2500);
  console.log('tests with large array');
  await runForTimes(5000, 100, 100);

  async function runForLength(times, start, step) {
    const results = [methodsnames];
    for (let i = 0, maxi = 10; i < maxi; i++) {
      const length = start + (step * i);
      results.push(length + ',' + (await test(times, length, methods)).join(','));
    }
    console.log(results.join('\n'));
  }
  console.log('tests with increasing length');
  await runForLength(5000, 100, 100);

  console.log('done');
})().catch(x => {
  console.error(x)
});